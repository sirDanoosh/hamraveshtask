import { ConfigProvider, Layout, Modal, Switch, theme } from "antd";
import React, { useEffect, useState } from "react";
import TaskContainer from "src/components/TaskContainer";
import { useAppDispatch, useAppSelector } from "src/store/hooks";
import { configModal } from "src/store/reducers/appSlice";
import { BulbOutlined } from "@ant-design/icons";
import "./app.scss";

const { darkAlgorithm, defaultAlgorithm } = theme;

const App: React.FC<{}> = () => {
  const [isLight, setIsLight] = useState(true);
  const [modal, contextHolder] = Modal.useModal();

  const dispatch = useAppDispatch();

  useEffect(() => {
    if (modal) {
      dispatch(configModal(modal));
    }
  }, [modal]);

  return (
    <ConfigProvider
      theme={{
        algorithm: isLight ? defaultAlgorithm : darkAlgorithm,
        token: {
          colorPrimary: "#4c3faf",
          colorText: "#4c3faf",
        },
      }}
    >
      <Layout>
        <Layout.Content className="content">
          <div
            className={`light-switch__wrapper ${isLight ? "light" : ""}`.trim()}
            onClick={() => setIsLight((prev) => !prev)}
          >
            <BulbOutlined
              className={`light-switch ${isLight ? "light" : ""}`.trim()}
            />
          </div>
          <TaskContainer />
          <Modal destroyOnClose />
          {contextHolder}
        </Layout.Content>
      </Layout>
    </ConfigProvider>
  );
};

export default App;
