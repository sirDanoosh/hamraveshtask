import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { ITaskBoard } from "src/types";
import TaskList from "../TaskList";
import "./style.scss";
import useTaskBoard from "./useTaskBoard";

const TaskBoard = (props: ITaskBoard) => {
  const { tasksList } = useTaskBoard(props);
  return (
    <div className="task-board">
      <DndProvider backend={HTML5Backend}>
        <TaskList title="To-Do" tasks={tasksList.todo} />
        <TaskList title="In Progress" tasks={tasksList.inprogress} />
        <TaskList title="Done" tasks={tasksList.done} />
      </DndProvider>
    </div>
  );
};

export default TaskBoard;
