import { useEffect, useState } from "react";
import { ITaskBoard, TASK_STATUS, TToDoList } from "src/types";

const useTaskBoard = (props: ITaskBoard) => {
  const { tasks } = props;
  const [tasksList, setTasksList] = useState<{
    todo: TToDoList;
    inprogress: TToDoList;
    done: TToDoList;
  }>({
    todo: [],
    inprogress: [],
    done: [],
  });

  useEffect(() => {
    if (tasks.length === 0) return;

    const todoTemp: TToDoList = [];
    const inProgressTemp: TToDoList = [];
    const doneTemp: TToDoList = [];

    tasks.forEach((task) => {
      switch (task.status) {
        case TASK_STATUS.DONE:
          return doneTemp.push(task);
        case TASK_STATUS.IN_PROGRESS:
          return inProgressTemp.push(task);
        case TASK_STATUS.TO_DO:
          return todoTemp.push(task);
      }
    });

    setTasksList({
      done: doneTemp,
      inprogress: inProgressTemp,
      todo: todoTemp,
    });
  }, [tasks]);

  return {
    tasksList,
  };
};

export default useTaskBoard;
