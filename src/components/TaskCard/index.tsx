import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Card } from "antd";
import { Txt } from "src/libs/Text";
import { TASK_STATUS, TODO_TYPE, TTaskCard } from "src/types";
import useTaskCard from "./useTaskCard";
import "./style.scss";

const TaskCard = (props: TTaskCard) => {
  const {
    task,
    editTaskModal,
    removeTaskConfirmModal,
    viewTask,
    drag,
    isDragging,
  } = useTaskCard(props);

  return task ? (
    <Card
      bordered
      title={
        <Txt type={task.type === TODO_TYPE.EASY ? "success" : "danger"} strong>
          {task.type === TODO_TYPE.EASY ? "easy" : "hard"}
        </Txt>
      }
      ref={drag}
      className="task-card"
      actions={[
        <EditOutlined
          key="edit"
          onClick={editTaskModal}
          className="task-card__icon"
        />,
        <DeleteOutlined
          key="remove"
          onClick={removeTaskConfirmModal}
          className="task-card__icon task-card__remove-icon"
        />,
      ]}
      style={{
        opacity: isDragging ? 0.5 : 1,
      }}
    >
      <Txt
        strong
        delete={task.status === TASK_STATUS.DONE}
        onClick={viewTask}
        style={{
          cursor: "pointer",
        }}
      >
        {task.title}
      </Txt>
    </Card>
  ) : null;
};

export default TaskCard;
