import { CloseCircleOutlined, DeleteOutlined } from "@ant-design/icons";
import TaskModalBody from "src/components/TaskModalBody";
import { Txt } from "src/libs/Text";
import { useAppDispatch, useAppSelector } from "src/store/hooks";
import {
  removeSelectedTask,
  removeTask,
  selectTask,
  setSelectedTask,
} from "src/store/reducers/todoListSlice";
import { TTaskCard, TToDo } from "src/types";
import { useDrag } from "react-dnd";

const useTaskCard = (props: TTaskCard) => {
  const { taskId, task } = props;
  const dispatch = useAppDispatch();
  const modal = useAppSelector((s) => s.appSlice.modal);
  const tsk = task
    ? task
    : (useAppSelector((s) => selectTask(s, taskId)) as TToDo);
  const tskId = task ? task.id : taskId;

  const [{ isDragging }, drag] = useDrag(
    () => ({
      type: "taskCard",
      collect: (monitor) => {
        return {
          isDragging: !!monitor.isDragging(),
        };
      },
      item: {
        ...tsk,
      },
    }),
    [tsk.id]
  );

  function editTaskModal() {
    if (!modal) return;
    dispatch(setSelectedTask(tsk));
    modal.info({
      title: (
        <>
          <Txt>Edit </Txt>
          <Txt strong style={{ fontSize: "16px" }}>
            {task?.title}
          </Txt>
        </>
      ),
      icon: null,
      footer: null,
      content: <TaskModalBody />,
    });
  }

  function viewTask() {
    if (!modal) return;
    dispatch(setSelectedTask(tsk));
    modal.info({
      title: (
        <>
          <Txt strong style={{ fontSize: "16px" }}>
            {task?.title}
          </Txt>
        </>
      ),
      icon: null,
      footer: null,
      closable: true,
      maskClosable: true,
      content: <TaskModalBody isView />,
    });
  }

  function removeTaskHandler() {
    dispatch(removeTask(tskId));
  }

  function removeTaskConfirmModal() {
    if (!modal) return;
    modal.confirm({
      title: (
        <>
          <Txt>Delete </Txt>
          <Txt type="warning" strong>
            {task?.title}
          </Txt>
        </>
      ),
      icon: <DeleteOutlined style={{ color: "red" }} />,
      okText: "Delete",
      cancelText: "Cancel",
      onOk: removeTaskHandler,
      onCancel: () => null,
      maskClosable: true,
      keyboard: true,
    });
  }

  return {
    task: tsk,
    removeTaskConfirmModal,
    editTaskModal,
    viewTask,
    drag,
    isDragging,
  };
};

export default useTaskCard;
