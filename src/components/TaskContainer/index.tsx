import TaskBoard from "src/components/TaskBoard";
import useTaskContainer from "./useTaskContainer";
import "./style.scss";
import { Button } from "antd";
import { Ttl } from "src/libs/Text";

const TaskContainer: React.FC<{}> = () => {
  const { tasks, createTask } = useTaskContainer();
  return (
    <div className="task-container">
      <Ttl level={2} className="task-container__title">
        TO-DO List
      </Ttl>
      <Button
        type="primary"
        onClick={createTask}
        className="task-container__create-btn"
      >
        Create Task
      </Button>
      <TaskBoard tasks={tasks} />
    </div>
  );
};

export default TaskContainer;
