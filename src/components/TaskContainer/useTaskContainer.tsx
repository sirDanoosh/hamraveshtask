import { CloseCircleOutlined } from "@ant-design/icons";
import TaskModalBody from "src/components/TaskModalBody";
import { Txt } from "src/libs/Text";
import { useAppSelector } from "src/store/hooks";

const useTaskContainer = () => {
  const tasks = useAppSelector((s) => s.todoSlice.tasks);
  const modal = useAppSelector((s) => s.appSlice.modal);

  function createTask() {
    if (!modal) return;
    modal.info({
      title: (
        <>
          <Txt>Create Task</Txt>
        </>
      ),
      icon: null,
      footer: null,
      content: <TaskModalBody />,
    });
  }

  return {
    tasks,
    createTask,
  };
};

export default useTaskContainer;
