import { Collapse, List } from "antd";
import React from "react";
import { Txt } from "src/libs/Text";
import { ITaskList } from "src/types";
import TaskCard from "../TaskCard";
import "./style.scss";
import useTaskList from "./useTaskList";

const TaskList = (props: ITaskList) => {
  const { tasks, title } = props;
  const { drop } = useTaskList({ title });
  return (
    <div className="task-list__wrapper" ref={drop}>
      <Collapse className="task-collapse">
        <Collapse.Panel header={<Txt strong>{title}</Txt>} key="1">
          <List
            className="task-collapse__list"
            dataSource={tasks}
            renderItem={(item) => (
              <List.Item>
                <TaskCard task={item} />
              </List.Item>
            )}
          />
        </Collapse.Panel>
      </Collapse>
      <List
        className="task-list"
        dataSource={tasks}
        header={<Txt strong>{title}</Txt>}
        renderItem={(item) => (
          <List.Item>
            <TaskCard task={item} />
          </List.Item>
        )}
      />
    </div>
  );
};

export default TaskList;
