import { modifyTask } from "src/store/reducers/todoListSlice";
import { useAppDispatch } from "./../../store/hooks";
import { useDrop } from "react-dnd";
import { TASK_STATUS, TToDo } from "src/types";

const useTaskList = (props: { title: string }) => {
  const { title } = props;
  const dispatch = useAppDispatch();

  function onDrop(task: TToDo) {
    dispatch(
      modifyTask({
        ...task,
        status:
          title === "To-Do"
            ? TASK_STATUS.TO_DO
            : title === "In Progress"
            ? TASK_STATUS.IN_PROGRESS
            : TASK_STATUS.DONE,
      })
    );
  }

  const [, drop] = useDrop(() => ({
    accept: "taskCard",
    drop: (item) => onDrop(item as TToDo),
  }));

  return {
    drop,
  };
};

export default useTaskList;
