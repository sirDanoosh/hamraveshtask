import { Button, Form, FormInstance, Input, Select } from "antd";
import { useRef } from "react";
import useTaskModalBody from "./useTaskModalBody";
import "./style.scss";

const TaskModalBody = ({ isView = false }) => {
  const formRef = useRef<FormInstance>(null);
  const { onFormSubmit, cancelFormHandler, isEdit, isEasy, changeType } =
    useTaskModalBody(formRef);

  return (
    <Form
      name="task-form"
      ref={formRef}
      onFinish={onFormSubmit}
      layout="vertical"
      preserve={false}
      style={{ maxWidth: "500px" }}
    >
      <div className="form__top">
        <Form.Item
          label="Title"
          name="title"
          htmlFor="title"
          rules={[
            {
              required: !isView && true,
              message:
                "Title must be provided and must not exceed 20 characters",
            },
          ]}
          style={{
            marginRight: "12px",
          }}
        >
          <Input
            disabled={isView}
            placeholder="task title (Max 20 characters)"
            maxLength={20}
            id="title"
          />
        </Form.Item>
        <Form.Item name="type" label="Task type" htmlFor="type">
          <Select
            onChange={changeType}
            disabled={isEdit || isView}
            defaultValue={"hard"}
            id="type"
          >
            <Select.Option value="easy">Easy</Select.Option>
            <Select.Option value="hard">Hard</Select.Option>
          </Select>
        </Form.Item>
      </div>
      <Form.Item
        name="description"
        label="Task description"
        htmlFor="description"
        required={!isView && !isEasy}
      >
        <Input.TextArea
          disabled={isView || isEasy}
          placeholder={
            isEasy
              ? "task description (only for hard tasks)"
              : "task description"
          }
          name="description"
          id="description"
        />
      </Form.Item>

      {!isView && (
        <div className="form__btns">
          <Button
            type="default"
            onClick={cancelFormHandler}
            className="form__btns__btn"
            htmlType="button"
          >
            Cancel
          </Button>
          <Button type="primary" className="form__btns__btn" htmlType="submit">
            {isEdit ? "Save changes" : "Create task"}
          </Button>
        </div>
      )}
    </Form>
  );
};

export default TaskModalBody;
