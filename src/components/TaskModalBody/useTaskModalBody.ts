import { useAppDispatch } from "src/store/hooks";
import { useEffect, useState } from "react";
import { FormInstance, Modal } from "antd";
import { useAppSelector } from "src/store/hooks";
import {
  modifyTask,
  addTask,
  removeSelectedTask,
} from "src/store/reducers/todoListSlice";
import { TASK_STATUS, TODO_TYPE } from "src/types";

const useTaskModalBody = (formRef: React.RefObject<FormInstance<any>>) => {
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [isEasy, setIsEasy] = useState<boolean>(false);
  const selectedTask = useAppSelector((s) => s.todoSlice.selectedTask);
  const dispatch = useAppDispatch();

  function onFormSubmit(values: any) {
    if (selectedTask) {
      dispatch(
        modifyTask({
          ...selectedTask,
          title: values.title,
          type: values.type,
          description: values.description,
        })
      );
    } else {
      dispatch(
        addTask({
          id: 0,
          status: TASK_STATUS.TO_DO,
          title: values.title,
          type: values.type,
          description: values.description,
        })
      );
    }
    dispatch(removeSelectedTask());
    setIsEasy(false);
    Modal.destroyAll();
  }

  function cancelFormHandler() {
    dispatch(removeSelectedTask());
    setIsEasy(false);
    Modal.destroyAll();
  }

  function changeType(value: "easy" | "hard") {
    setIsEasy(value === "easy");
  }

  useEffect(() => {
    if (selectedTask) {
      setIsEdit(true);
      setIsEasy(selectedTask.type === TODO_TYPE.EASY);
      formRef.current?.setFieldsValue({
        title: selectedTask.title,
        type: selectedTask.type,
        description: selectedTask.description ?? "",
      });
    } else {
      setIsEdit(false);
    }
  }, [selectedTask]);

  return {
    onFormSubmit,
    cancelFormHandler,
    changeType,
    isEdit,
    isEasy,
  };
};

export default useTaskModalBody;
