import { TextProps } from "antd/es/typography/Text";
import { TitleProps } from "antd/es/typography/Title";
import { Typography } from "antd";

const { Text, Title } = Typography;

export const Txt = (props: TextProps) => {
  return <Text {...props} />;
};

export const Ttl = (props: TitleProps) => {
  return <Title {...props} />;
};
