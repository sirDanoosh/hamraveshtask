import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ModalStaticFunctions } from "antd/es/modal/confirm";

const initialState: {
  modal: Omit<ModalStaticFunctions, "warn"> | null;
} = {
  modal: null,
};

export const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    configModal: (
      state,
      action: PayloadAction<Omit<ModalStaticFunctions, "warn">>
    ) => {
      state.modal = action.payload;
    },
  },
});

export const { configModal } = appSlice.actions;

export default appSlice.reducer;
