import appSlice from "src/store/reducers/appSlice";
import todoListSlice from "src/store/reducers/todoListSlice";
import { combineReducers } from "@reduxjs/toolkit";

const rootReducer = combineReducers({
  appSlice,
  todoSlice: todoListSlice,
});

export default rootReducer;
