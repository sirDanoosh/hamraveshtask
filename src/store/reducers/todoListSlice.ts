import type { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";
import type { RootState } from "src/store/store";
import { TASK_STATUS, TODO_TYPE, TToDo, TToDoList } from "src/types";

const initialState: { tasks: TToDoList; selectedTask: TToDo | null } = {
  tasks: [
    {
      id: 1,
      status: TASK_STATUS.TO_DO,
      title: "Do the dishes",
      type: TODO_TYPE.HARD,
      description: "needed before cooking next meal",
    },
    {
      id: 2,
      status: TASK_STATUS.TO_DO,
      title: "Launch Rocket",
      type: TODO_TYPE.EASY,
    },
    {
      id: 3,
      status: TASK_STATUS.TO_DO,
      title: "Homework",
      type: TODO_TYPE.EASY,
    },
    {
      id: 4,
      status: TASK_STATUS.IN_PROGRESS,
      title: "Task2",
      type: TODO_TYPE.HARD,
      description: "before task1",
    },
    {
      id: 5,
      status: TASK_STATUS.DONE,
      title: "Call Jafar",
      type: TODO_TYPE.HARD,
      description: "find Jafar number",
    },
    {
      id: 6,
      status: TASK_STATUS.DONE,
      title: "Learn how to fly",
      type: TODO_TYPE.HARD,
      description: "need to provide wings",
    },
    {
      id: 7,
      status: TASK_STATUS.DONE,
      title: "Task4",
      type: TODO_TYPE.EASY,
    },
  ],
  selectedTask: null,
};

export const todoListSlice = createSlice({
  name: "todoList",
  initialState,
  reducers: {
    resetList: (state) => {
      state.tasks = [];
    },
    addTask: (state, action: PayloadAction<TToDo>) => {
      state.tasks = [...state.tasks, action.payload].map((item, idx) => {
        return {
          ...item,
          id: idx + 1,
        };
      });
    },
    removeTask: (state, action: PayloadAction<number>) => {
      state.tasks = state.tasks
        .filter((task) => task.id !== action.payload)
        .map((item, idx) => {
          return {
            ...item,
            id: idx + 1,
          };
        });
    },
    modifyTask: (state, action: PayloadAction<TToDo>) => {
      state.tasks = state.tasks.map((task) => {
        if (task.id === action.payload.id) {
          return { ...action.payload };
        } else {
          return { ...task };
        }
      });
    },
    setSelectedTask: (state, action: PayloadAction<TToDo>) => {
      state.selectedTask = action.payload;
    },
    removeSelectedTask: (state) => {
      state.selectedTask = null;
    },
  },
});

export const {
  resetList,
  addTask,
  removeTask,
  modifyTask,
  setSelectedTask,
  removeSelectedTask,
} = todoListSlice.actions;

export const selectTask = (state: RootState, taskId: number) =>
  state.todoSlice.tasks.find((task) => task.id === taskId);

export default todoListSlice.reducer;
