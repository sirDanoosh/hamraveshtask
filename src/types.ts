export enum TODO_TYPE {
  HARD = "hard",
  EASY = "easy",
}

export enum TASK_STATUS {
  TO_DO = "toDo",
  IN_PROGRESS = "inProgress",
  DONE = "done",
}

export interface IToDoHard {
  id: number;
  title: string;
  description: string;
  type: TODO_TYPE.HARD;
  status: TASK_STATUS;
}

export interface IToDoEasy {
  id: number;
  title: string;
  description?: null;
  type: TODO_TYPE.EASY;
  status: TASK_STATUS;
}

export type TToDo = IToDoHard | IToDoEasy;

export type TToDoList = Array<TToDo>;

export type TTaskCard =
  | {
      taskId: number;
      task?: undefined;
    }
  | {
      task: TToDo;
      taskId?: undefined;
    };

export interface ITaskList {
  title: string;
  tasks: TToDoList;
}

export interface ITaskBoard {
  tasks: TToDoList;
}
